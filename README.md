# openstack-neutron-cern-distgit

This repository takes care of the RPM distribution of [openstack-neutron-cern](https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern).

## Conventions

* The repository contains **a branch per OpenStack release**. This is handy to target different repositories where the software will be distributed, and potentially distribute the software to multiples repos simultaneously.
* The distribution logic assumes that the source repository has a release tag in the form `v<semver>` (e.g. `0.13.0`, `0.14.2`).

## Procedures

### Bump a release within the same OpenStack release

When there is a new version to release, you can follow these basic steps:

* Create a new branch to modify the `.spec` file with the new version and changelog details.
* Create a merge request and let the pipeline run to validate the RPM builds succesfully.
* Once merged, tag the `master` branch with the new release version (e.g. `0.13.0-1`).
* Once tagged, follow the associated pipeline to release to `qa` and to `stable` once the package has run in QA.

### Release to a newer OpenStack release

* Branch out to target the new tag (e.g. `train`).
* Create a new development branch and fix `.gitlab-ci.yml` to point to the right tags, as well as the `.spec` file as required.
* Create a merge request and validate the builds.
* Once merged, tag the `master` branch with the new release version (e.g. `0.13.0-1`).
* Once tagged, follow the associated pipeline to release to `qa` and to `stable` once the package has run in QA.

## Known limitations

The current logic assumes that source respository [openstack-neutron-cern](https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern) is configured to be public. Limiting access to the repo would require to modify `Makefile` to make an authenticated call to fetch the source.
