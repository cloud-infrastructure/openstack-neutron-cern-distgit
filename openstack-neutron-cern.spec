%define pkg networking_cern

Name:       openstack-neutron-cern
Version:    0.15.3
Release:    1%{?dist}
Summary:    CERN Networking plugin

Group:      Development/Languages
License:    ASL 2.0
Url:        https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern
Source0:    https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern/-/archive/v%{version}/openstack-neutron-cern-v%{version}.tar.gz

BuildArch: noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
Requires:       python3-ldap
Requires:       python3-glanceclient
Requires:       openstack-neutron >= 15.0.0
Requires:       python-landbclient >= 0.20.0-1

%description
Cern networking Plugin with landb support.

%prep
%setup -q -n %{name}-v%{version}

%build
%py3_build

%install
%py3_install

%files
%doc README.rst
%{python3_sitelib}/%{pkg}
%{python3_sitelib}/*.egg-info

%changelog
* Tue Apr 02 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.3-1
- Add support for querying segments API to get LanDB cluster information

* Fri Nov 03 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.2-1.2
- Initial release for yoga

* Thu Jul 06 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 0.15.2-1.1
- Initial release for yoga

* Fri Feb 17 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.2-1
- Rename tenant_id column in cern_clusters to project_id
- Configuration parameter xldap_url to customise the LDAP endpoint to use (OS-16344)

* Mon Jan 23 2023 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.1-2
- Build for RHEL8 & ALMA8

* Fri Oct  7 2022 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.1-1
- Replace deprecated "auth-uri" by "www_authenticate_uri" (fernandl, #60)

* Tue Aug 23 2022 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.0-2
- Build with Python 3 for major releases greater than 7
- Set dependencies properly for build & runtime

* Fri Jun 24 2022 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.15.0-1
- Adapted to OpenStack Neutron Train (fernandl, #57)

* Thu Jun 23 2022 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.14.0-1
- Added most_available_subnet_v{4,6} to host restrictions API (fernandl, #54)

* Thu May 19 2022 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 0.13.1-1
- Changed type of option for verify (jcastro, #51)
- Fixed UnboundLocalError: local variable instance referenced before assignment (jcastro, #51)

* Mon May 31 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 0.13.0-2
- Update dependency on python-landbclient 

* Mon May 31 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 0.13.0-1
- Add support for landb sets

* Wed Jan 27 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.6-1
- Allow insecure connections to openstack services

* Wed Jan 27 2021 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.5-1
- Release bump for landbclient dep in dockerfile

* Wed Aug 14 2019 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.4-1
- Use initialize instead of open (deprecated) in ldap calls 

* Tue Apr 02 2019 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.3-1
- Log hostname when failing to find cluster for host

* Wed Feb 27 2019 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.2-1
- Add region to nova and neutron clients

* Wed Sep 19 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.12.1-1
- Include method used in mech_landb exceptions

* Wed Aug 22 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.12.0-1
- Add region_name param to be able to select the proper region
- Pylint requires python3 set pylint to run under python2

* Thu Apr 19 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.11.1-1
- Code refactoring neutron.plugins.ml2.driver_api to neutron_lib.api

* Mon Feb 12 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.10.1-1
- Fix handling alias in vm creation

* Mon Feb 12 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.10.0-1
- Add handling of alias in vm creation

* Thu Feb 08 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.9.1-1
- Fix delete first instance of a device_search list

* Thu Jan 25 2018 Ricardo Rocha<ricardo.rocha@cern.ch> 0.9.0-1
- Adapt plugin and extensions to neutron Ocata release

* Wed Oct 18 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.8.4-1
- Fix Handle landb-ipv6ready on port creation

* Wed Oct 18 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.8.3-1
- Handle landb-ipv6ready on port creation

* Tue Aug 29 2017 Ricardo Rocha <ricardo.rocha@cenr.ch> 0.8.2-1
- Handle landb-internet-connectivity on port creation

* Tue Aug 29 2017 Ricardo Rocha <ricardo.rocha@cern.ch> 0.8.1-1
- Do not try to lock vm after creation (replaced with landb manager in v6)
- Always return the a ip6 subnet beloging to the same ip4 subnet service

* Mon Aug 28 2017 Ricardo Rocha <ricardo.rocha@cern.ch> 0.8.0-1
- Handle image metadata when booting from image (creates a volume)

* Fri Aug 25 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.0-1
- Add support for IPv6 and adapt to landb SOAPv6 API

* Thu Aug 17 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.6.0-1
- Make configurable landbManager in config file

* Tue Aug 15 2017 Ricardo Rocha <ricardo.rocha@cern.ch> 0.5.0-1
- Refactor mech landb driver, split into smaller methods
- Add pylint compliance
- Add functional tests for vm/port creation and landb metadata management

* Fri Jul 28 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.4.1-1
- Fix subnet cluster exceptions

* Fri Jul 28 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.4.0-1
- Fix LanDB version parameter with a correct input

* Fri Jul 28 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.3.5-1
- Fix import config params from a config file

* Wed Jun 28 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.3.4-1
- Add LanDB version parameter
