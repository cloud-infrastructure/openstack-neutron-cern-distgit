SPECFILE            = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME       = $(shell awk '$$1 == "Name:"        { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION    = $(shell awk '$$1 == "Version:"     { print $$2 }' $(SPECFILE) )
DIST               ?= $(shell rpm --eval %{dist})

source:
	wget https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern/-/archive/v$(SPECFILE_VERSION)/$(SPECFILE_NAME)-v$(SPECFILE_VERSION).tar.gz

rpm: source
	rpmbuild -bb --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)

srpm: source
	rpmbuild -bs --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)
